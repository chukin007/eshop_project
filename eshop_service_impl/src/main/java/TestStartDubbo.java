import com.alibaba.dubbo.container.Main;

/**
 * @author sunhongmin 
 * dubbo启动类
 */
public class TestStartDubbo {
	public static void main(String[] args) {
		Main.main(args);//启动dubbo
	}
}
