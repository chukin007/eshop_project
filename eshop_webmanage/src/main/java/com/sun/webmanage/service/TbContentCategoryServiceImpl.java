package com.sun.webmanage.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.commons.pojo.EasyUITreeData;
import com.sun.dubbo.webmanage.service.TbContentCategoryDubboService;
import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbContentCategory;

@Service("TbContentCategoryService")
public class TbContentCategoryServiceImpl implements TbContentCategoryService{

	@Reference
	private TbContentCategoryDubboService tbContentCategoryDubboService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TbContentCategoryService.class);

	@Override
	public List<EasyUITreeData> listTbContentCategory(long pid) {
		List<TbContentCategory> listdata = tbContentCategoryDubboService.listContentCatByPid(pid);
		List<EasyUITreeData> retdata = new ArrayList<>();
		EasyUITreeData data = null;
		for(TbContentCategory tbcate:listdata){
			data = new EasyUITreeData();
			data.setId(tbcate.getId());
			data.setText(tbcate.getName());
			data.setState(tbcate.getIsParent()?"closed":"open");
			retdata.add(data);
		}
		return retdata;
	}

	@Override
	public TbContentCategory saveTbContentCategory(Long parentId,String name) throws DaoException{
		TbContentCategory category = new TbContentCategory();
		category.setName(name);
		category.setParentId(parentId);
		category.setStatus(1);
		Date now = new Date();
		category.setCreated(now);
		category.setUpdated(now);
		category.setIsParent(false);
		TbContentCategory insertObj = tbContentCategoryDubboService.insertTbContentCategory(category,parentId);
	    return insertObj;
	}

	@Override
	public boolean updateTbContentCategory(Long id,String name) throws DaoException {
		TbContentCategory category = new TbContentCategory();
		category.setId(id);
		category.setName(name);
		category.setUpdated(new Date());
		if(tbContentCategoryDubboService.updateTbContentCategory(category)>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteTbContentCategory(Long parentId,Long id) {
		try {
			boolean status = tbContentCategoryDubboService.deleteTbContentCategory(parentId,id);
			return status;
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error("deleteTbContentCategory error==="+e.getMessage());
			return false;
		}
	}
	
	
	
}
