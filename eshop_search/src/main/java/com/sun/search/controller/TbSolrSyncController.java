package com.sun.search.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sun.commons.pojo.SolrInsertData;
import com.sun.commons.utils.ActionUtils;
import com.sun.search.service.SolrSynService;

@RestController
public class TbSolrSyncController {

	@Resource
	private SolrSynService solrSynService;
	
	@RequestMapping("/solr/init")
	public Map<String,Object> initSolrSync(@RequestParam(value="is_all",defaultValue="false")boolean is_all){
		//{"data":"更新条目数：3500,操作时间：77s","status":200,"info":"同步solr索引信息成功"}
		//大约   10k/4min
		return solrSynService.initData(is_all);
	}
	
	@RequestMapping("/search/add")
	public Map<String,Object> searchDataAdd(@RequestBody SolrInsertData solrInsertData){
		boolean insertData = solrSynService.insertData(solrInsertData);
		if(insertData){
			return ActionUtils.ajaxSuccess("数据同步成功", "");
		}
		
		return ActionUtils.ajaxFail("数据同步失败", "");
	}
	
	@RequestMapping("/search/update")
	public Map<String,Object> searchDataEdit(@RequestBody SolrInsertData solrInsertData){
		boolean updateData = solrSynService.updateData(solrInsertData);
		if(updateData){
			return ActionUtils.ajaxSuccess("数据索引更新成功", "");
		}
		
		return ActionUtils.ajaxFail("数据索引更新失败", "");
	}
	
	@RequestMapping("/search/del")
	public Map<String,Object> searchDataDel(@RequestBody List<String> ids){
		boolean deleteDate = solrSynService.deleteDate(ids);
		if(deleteDate){
			return ActionUtils.ajaxSuccess("数据索引删除成功", "");
		}
		
		return ActionUtils.ajaxFail("数据索引删除失败", "");
	}
	
}
