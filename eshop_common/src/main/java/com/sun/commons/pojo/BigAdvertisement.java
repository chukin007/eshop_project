package com.sun.commons.pojo;

import java.io.Serializable;

/**
 * @author 孙哈哈
 * 前台大广告Bean
 */
public class BigAdvertisement implements Serializable{

	/**
	 * "srcB":"http://image.ego.com/images/2015/03/03/2015030304360302109345.jpg",
        "widthB":550,
        "heightB":240
        "width":670,
        "height":240,
        "src":"http://image.ego.com/images/2015/03/03/2015030304360302109345.jpg",
        "alt":"",
        "href":"http://sale.jd.com/act/e0FMkuDhJz35CNt.html?cpdad=1DLSUE",
	 * */
	private long id;//缓存标识
	
	private String src;//图片地址
	private String alt;//鼠标悬浮时标题
	private String href;//点击跳转链接
	private double width;//宽
	private double height;//高
	private String srcB;//备选图片
	private double widthB;//备选宽
	private double heightB;//备选高
	
	public BigAdvertisement(){
		//宽高默认值
		this.width = 670;
		this.widthB = 550;
		this.height = 240;
		this.heightB = 240;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getAlt() {
		return alt;
	}
	public void setAlt(String alt) {
		this.alt = alt;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getSrcB() {
		return srcB;
	}
	public void setSrcB(String srcB) {
		this.srcB = srcB;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWidthB() {
		return widthB;
	}
	public void setWidthB(double widthB) {
		this.widthB = widthB;
	}
	public double getHeightB() {
		return heightB;
	}
	public void setHeightB(double heightB) {
		this.heightB = heightB;
	}
}
